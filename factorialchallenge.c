/* factorialchallenge.c 
	Author: David Warren II
	Date: 20 February, 2017
	Description: Some factorial challenge to practice programming in C.
		Requests an integer, prints its factorial, outputs how many
		trailing zeros.

	*** WARNING: there is no error handling for non-integers or when factorial
		becomes too big ***
*/

#include <stdio.h>

long int factorial(int number);
int zeroCheck(long int number, int place);

int main()
{
	int number = 0;

	printf("Enter an integer: ");
	scanf("%d", &number);

	printf("The factorial of %d is: %ld\n", number, factorial(number));
	printf("This number has %d zero(s)\n", zeroCheck(factorial(number), 1));

	return 0;
}

long int factorial(int number)
{
	if(number > 1)
		return number * factorial(number - 1);		
	else
		return number;
}

int zeroCheck(long int number, int place)
{
	if((number % (10 * place)) == 0) {
		return 1 + zeroCheck(number, place *= 10);
	}
	else
		return 0;
}
