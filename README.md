README

Just a quick factorial(recursive) program. 

How to run:
gcc -o output factorialchallenge.c
./output

What it does:
It inputs an integer from stdin, calculates the factorial(recursively), and then it outputs how many TRAILING zeros the number has. 
Both are calculated recursively to reduce calculation time. *** WARNING: there is no error handling for non-integers or factorials that get too big.